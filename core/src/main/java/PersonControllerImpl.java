import java.util.ArrayList;
import java.util.List;

public class PersonControllerImpl implements PersonController{

    private final List<Person> people = new ArrayList<>();

    @Override
    public boolean doesPersonExist(Person person) {
       return people.contains(person);
    }

    @Override
    public void addNewPerson(Person person) {
        people.add(person);
    }

    @Override
    public List<Person> getPeopleList() {
        return people;
    }
}
