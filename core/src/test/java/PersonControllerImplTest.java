import org.junit.Before;
import org.junit.Test;

import java.time.LocalDate;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class PersonControllerImplTest {
    PersonControllerImpl personController = new PersonControllerImpl();

    Person nonexistentPerson;
    Person existentPerson;
    Person personToAdd;

    @Before
    public void init() {
        initPeople();
        prepopulatePeople();
    }

    @Test
    public void checkExistingPersonIfHeDoesNotExist() {
       boolean isExist = personController.doesPersonExist(nonexistentPerson);
       assertFalse(isExist);
    }

    @Test
    public void checkExistingPersonIfHeExists() {
        boolean isExist = personController.doesPersonExist(existentPerson);
        assertTrue(isExist);
    }

    @Test
    public void addNewPerson() {
        int sizeBefore = personController.getPeopleList().size();
        personController.addNewPerson(personToAdd);
        int sizeAfter = personController.getPeopleList().size();

        boolean sizeIncreased = sizeBefore + 1 == sizeAfter;
        assertTrue(sizeIncreased);

        boolean personExists = personController.doesPersonExist(personToAdd);
        assertTrue(personExists);
    }

    private void initPeople() {
        existentPerson = new Person("Kyla", "Fitzpatrick", "Kylie",
                Sex.FEMALE, LocalDate.of(1976, 7, 15));
        nonexistentPerson = new Person("Julia", "Miller", "Robert",
                Sex.FEMALE, LocalDate.of(1976, 7, 15));
        personToAdd = new Person("Roman", "Arnold", "John",
                Sex.FEMALE, LocalDate.of(1976, 7, 15));
    }

    private void prepopulatePeople() {
        personController.addNewPerson(new Person("Sergey", "Bell", "Anise",
                Sex.MALE, LocalDate.of(2000, 5, 17)));
        personController.addNewPerson(new Person("Talia", "Welch", "Nevin",
                Sex.FEMALE, LocalDate.of(1985, 1, 3)));
        personController.addNewPerson(new Person("Jakob", "Zimmerman", "Thomas",
                Sex.MALE, LocalDate.of(1968, 9, 10)));
        personController.addNewPerson(new Person("Kiersten", "Kelley", "Justin",
                Sex.FEMALE, LocalDate.of(2003, 12, 26)));
        personController.addNewPerson(existentPerson);
    }
}
