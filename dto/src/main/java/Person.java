import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Objects;

public class Person {
    private String name;
    private String lastName;
    private String secondName;
    private int age;
    private Sex sex;
    private LocalDate birthday;

    public Person(String name, String lastName, String secondName, Sex sex, LocalDate birthday) {
        this.name = name;
        this.lastName = lastName;
        this.secondName = secondName;
        this.sex = sex;
        this.birthday = birthday;
        this.age = (int) ChronoUnit.YEARS.between(birthday, LocalDate.now());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return age == person.age
                && Objects.equals(name, person.name)
                && Objects.equals(lastName, person.lastName)
                && Objects.equals(secondName, person.secondName)
                && sex == person.sex
                && Objects.equals(birthday, person.birthday);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, lastName, secondName, age, sex, birthday);
    }
}


