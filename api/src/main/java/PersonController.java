import java.util.List;

public interface PersonController {

    boolean doesPersonExist(Person personToFind);

    void addNewPerson(Person person);

    List<Person> getPeopleList();
}
